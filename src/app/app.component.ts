import {Component, OnInit, ViewChild} from '@angular/core';
import {Injectable, isDevMode} from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {HttpClient, HttpHeaders} from "@angular/common/http";

interface ServerRes {
  status: string;
  msg: string;
  data: any;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  displayedColumns: string[] = ['id_ruta', 'driver_name', 'car_desc', 'number_clients', 'number_travels'];
  dataSource = new MatTableDataSource<responseInterface>(ELEMENT_DATA);
  file = null;
  response = null;
  dataFiltered = null;
  constructor(private http: HttpClient) {  };

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.file = null;
    this.response = 'Esperando  carga de archivos';
    this.dataFiltered = 'Esperando  petición de archivos';
    this.dataSource.paginator = this.paginator;
  }
  onFileChange (event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      console.log(file);
      this.file = file;
    }
  }

  sendFile () {
    let headers = this.createRequestOptions();
    console.log(this.file);
    let data = new FormData();
    data.append('file', this.file);
    this.http.post<ServerRes>('http://192.168.10.220/import',
      data, { headers: headers,  withCredentials: true })
      .subscribe((res) => {
        if (res['status'] === 'OK') {
          alert(res['msg']);
          this.response = res['msg'];
        } else if (res['status'] === 'ERROR') {
          alert(res['msg']);
          this.response = res['msg'];
        }
      }, err => {
        console.log(err);
        this.response = 'fallo el servidor';
        alert('fallo el servidor')
      })

  }
  getFiles () {
    let headers = this.createRequestOptions();
    console.log(this.file);
    let data = new FormData();
    data.append('file', this.file);
    this.http.get<ServerRes>('http://192.168.10.220/datatable', { headers: headers,  withCredentials: true })
      .subscribe((res) => {
        if (res['status'] === 'OK') {
          alert(res['msg']);
          this.dataFiltered = res['data'];
        } else if (res['status'] === 'ERROR') {
          alert(res['msg']);
          this.dataFiltered = null;
        }
      }, err => {
        console.log(err);
        this.dataFiltered = 'fallo el servidor';
        alert('fallo el servidor')
      })

  }
  private createRequestOptions() {
    let headers = new HttpHeaders({
      "X-Requested-With": "XMLHttpRequest"
    });
    return headers;
  }
}

export interface responseInterface {
  id_ruta: string,
  driver_name: string,
  car_desc: string,
  number_clients: number
  number_travels: number
}

const ELEMENT_DATA: responseInterface[] = [
  {
    id_ruta: '998434',
    driver_name: 'Felix Ugarte',
    car_desc: 'MFS-123',
    number_clients: 503,
    number_travels: 34
  },
  {
    id_ruta: '77772',
    driver_name: 'Miguel Guzman',
    car_desc: 'XXX-123',
    number_clients: 50,
    number_travels: 23
  },
  {
    id_ruta: 'F34332',
    driver_name: 'Josue Manuel Segura',
    car_desc: 'MKJ-123',
    number_clients: 50,
    number_travels: 4
  },
  {
    id_ruta: '909090',
    driver_name: 'Ignacio Fuentes',
    car_desc: 'TRE-123',
    number_clients: 50,
    number_travels: 33
  },
  {
    id_ruta: 'A1234',
    driver_name: 'Alex Curo',
    car_desc: 'DCS-123',
    number_clients: 50,
    number_travels: 3
  },
  {
    id_ruta: '23412',
    driver_name: 'Mario Guzman',
    car_desc: 'FRE-123',
    number_clients: 50,
    number_travels: 23
  },
  {
    id_ruta: 'X3432',
    driver_name: 'Jose Segura',
    car_desc: 'DFE-123',
    number_clients: 50,
    number_travels: 4
  },
  {
    id_ruta: '62324',
    driver_name: 'Luis Fuentes',
    car_desc: 'ABC-123',
    number_clients: 50,
    number_travels: 33
  }
];
